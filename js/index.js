"use strict";
let peopleArray = [25, 25, 25];
let backlogArray = [150, 150, 150];
let deadlineDate = new Date(2021, 2, 19);
function deadlineCalculating(peopleArray, backlogArray, deadlineDate) {
    let beginDate = new Date();
    let peopleStoryPoints = 0;
    let backlogStoryPoints = 0;
    for (let key in peopleArray) {
        peopleStoryPoints += peopleArray[key];
    }
    for (let key in backlogArray) {
        backlogStoryPoints += backlogArray[key];
    }
    let remainder = backlogStoryPoints;
    for (let i = beginDate.getDate(); i < deadlineDate.getDate(); i++) {
        beginDate.setDate(beginDate.getDate() + 1);
        if (beginDate.getDay() === 6 || beginDate.getDay() === 0) {
            beginDate.setDate(beginDate.getDate() + 1);
            continue;
        }
        remainder -= peopleStoryPoints;
        console.log(`${i} - ${remainder} стори поинтов осталось выполнить`);
        if (remainder <= 0) {
            console.log(`Все задачи будут успешно выполнены за ${deadlineDate.getDate() - i} дней до наступления дедлайна!`);
            return true;
        }
    }
    console.log(`Команде разработчиков придется потратить дополнительно ${remainder / peopleStoryPoints * 8} часов после дедлайна, чтобы выполнить все задачи в беклоге`);
}
deadlineCalculating(peopleArray, backlogArray, deadlineDate);